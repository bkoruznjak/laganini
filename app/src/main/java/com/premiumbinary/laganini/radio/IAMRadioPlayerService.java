package com.premiumbinary.laganini.radio;

/**
 * Created by borna on 15.01.17..
 */


import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.spoledge.aacdecoder.IcyURLStreamHandler;
import com.spoledge.aacdecoder.MultiPlayer;
import com.spoledge.aacdecoder.PlayerCallback;

import java.net.URL;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import co.mobiwise.library.R.drawable;
import co.mobiwise.library.radio.RadioListener;
import co.mobiwise.library.radio.StreamLinkDecoder;

public class IAMRadioPlayerService extends Service implements PlayerCallback {
    public static final String ACTION_MEDIAPLAYER_STOP = "co.mobiwise.library.ACTION_STOP_MEDIAPLAYER";
    private static final String NOTIFICATION_INTENT_PLAY_PAUSE = "co.mobiwise.library.notification.radio.INTENT_PLAYPAUSE";
    private static final String NOTIFICATION_INTENT_CANCEL = "co.mobiwise.library.notification.radio.INTENT_CANCEL";
    private static final String NOTIFICATION_INTENT_OPEN_PLAYER = "co.mobiwise.library.notification.radio.INTENT_OPENPLAYER";
    private static final int NOTIFICATION_ID = 1;
    private static boolean isLogging = false;
    public final IBinder mLocalBinder;
    private final int AUDIO_BUFFER_CAPACITY_MS;
    private final int AUDIO_DECODE_CAPACITY_MS;
    private final String SUFFIX_PLS;
    private final String SUFFIX_RAM;
    private final String SUFFIX_WAX;
    List<RadioListener> mListenerList;
    PhoneStateListener phoneStateListener;
    private String singerName = "";
    private String songName = "";
    private int smallImage;
    private Bitmap artImage;
    private IAMRadioPlayerService.State mRadioState;
    private String mRadioUrl;
    private MultiPlayer mRadioPlayer;
    private TelephonyManager mTelephonyManager;
    private boolean isSwitching;
    private boolean isClosedFromNotification;
    private boolean isInterrupted;
    private boolean mLock;
    private NotificationManager mNotificationManager;

    public IAMRadioPlayerService() {
        this.smallImage = drawable.default_art;
        this.AUDIO_BUFFER_CAPACITY_MS = 800;
        this.AUDIO_DECODE_CAPACITY_MS = 400;
        this.SUFFIX_PLS = ".pls";
        this.SUFFIX_RAM = ".ram";
        this.SUFFIX_WAX = ".wax";
        this.isClosedFromNotification = false;
        this.mLocalBinder = new IAMRadioPlayerService.LocalBinder();
        this.phoneStateListener = new PhoneStateListener() {
            public void onCallStateChanged(int state, String incomingNumber) {
                if(state == 1) {
                    if(IAMRadioPlayerService.this.isPlaying()) {
                        IAMRadioPlayerService.this.isInterrupted = true;
                        IAMRadioPlayerService.this.stop();
                    }
                } else if(state == 0) {
                    if(IAMRadioPlayerService.this.isInterrupted) {
                        IAMRadioPlayerService.this.play(IAMRadioPlayerService.this.mRadioUrl);
                    }
                } else if(state == 2 && IAMRadioPlayerService.this.isPlaying()) {
                    IAMRadioPlayerService.this.isInterrupted = true;
                    IAMRadioPlayerService.this.stop();
                }

                super.onCallStateChanged(state, incomingNumber);
            }
        };
    }

    public IBinder onBind(Intent intent) {
        return this.mLocalBinder;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if(action.equals("co.mobiwise.library.notification.radio.INTENT_CANCEL")) {
            if(this.isPlaying()) {
                this.isClosedFromNotification = true;
                this.stop();
            }

            if(this.mNotificationManager != null) {
                this.mNotificationManager.cancel(1);
            }
        } else if(action.equals("co.mobiwise.library.notification.radio.INTENT_PLAYPAUSE")) {
            if(this.isPlaying()) {
                this.stop();
            } else if(this.mRadioUrl != null) {
                this.play(this.mRadioUrl);
            }
        }

        return START_STICKY;
    }

    public void onCreate() {
        super.onCreate();
        this.mListenerList = new ArrayList();
        this.mRadioState = IAMRadioPlayerService.State.IDLE;
        this.isSwitching = false;
        this.isInterrupted = false;
        this.mLock = false;
        this.getPlayer();
        this.mTelephonyManager = (TelephonyManager)this.getSystemService(TELEPHONY_SERVICE);
        this.mNotificationManager = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
        if(this.mTelephonyManager != null) {
            this.mTelephonyManager.listen(this.phoneStateListener, 32);
        }

    }

    public void play(String mRadioUrl) {
        this.sendBroadcast(new Intent("co.mobiwise.library.ACTION_STOP_MEDIAPLAYER"));
        this.notifyRadioLoading();
        if(this.checkSuffix(mRadioUrl)) {
            this.decodeStremLink(mRadioUrl);
        } else {
            this.mRadioUrl = mRadioUrl;
            this.isSwitching = false;
            if(this.isPlaying()) {
                this.log("Switching Radio");
                this.isSwitching = true;
                this.stop();
            } else if(!this.mLock) {
                this.log("Play requested.");
                this.mLock = true;
                this.getPlayer().playAsync(mRadioUrl);
            }
        }

    }

    public void stop() {
        if(!this.mLock && this.mRadioState != IAMRadioPlayerService.State.STOPPED) {
            this.log("Stop requested.");
            this.mLock = true;
            this.getPlayer().stop();
        }

    }

    public void playerStarted() {
        this.mRadioState = IAMRadioPlayerService.State.PLAYING;
        this.mLock = false;
        this.notifyRadioStarted();
        this.log("Player started. tate : " + this.mRadioState);
        if(this.isInterrupted) {
            this.isInterrupted = false;
        }

    }

    public boolean isPlaying() {
        return IAMRadioPlayerService.State.PLAYING == this.mRadioState;
    }

    public void playerPCMFeedBuffer(boolean b, int i, int i1) {
    }

    public void playerStopped(int i) {
        this.mRadioState = IAMRadioPlayerService.State.STOPPED;
        if (this.isClosedFromNotification) {
            this.isClosedFromNotification = false;
        }

        this.mLock = false;
        this.notifyRadioStopped();
        this.log("Player stopped. State : " + this.mRadioState);
        if(this.isSwitching) {
            this.play(this.mRadioUrl);
        }

    }

    public void playerException(Throwable throwable) {
        this.mLock = false;
        this.mRadioPlayer = null;
        this.getPlayer();
        this.notifyErrorOccured();
        this.log("ERROR OCCURED.");
    }

    public void playerMetadata(String s, String s2) {
        this.notifyMetaDataChanged(s, s2);
    }

    public void playerAudioTrackCreated(AudioTrack audioTrack) {
    }

    public void registerListener(RadioListener mListener) {
        this.mListenerList.add(mListener);
    }

    public void unregisterListener(RadioListener mListener) {
        this.mListenerList.remove(mListener);
    }

    private void notifyRadioStarted() {
        Iterator i$ = this.mListenerList.iterator();

        while(i$.hasNext()) {
            RadioListener mRadioListener = (RadioListener)i$.next();
            mRadioListener.onRadioStarted();
        }

    }

    private void notifyRadioStopped() {
        Iterator i$ = this.mListenerList.iterator();

        while(i$.hasNext()) {
            RadioListener mRadioListener = (RadioListener)i$.next();
            mRadioListener.onRadioStopped();
        }

    }

    private void notifyMetaDataChanged(String s, String s2) {
        Iterator i$ = this.mListenerList.iterator();

        while(i$.hasNext()) {
            RadioListener mRadioListener = (RadioListener)i$.next();
            mRadioListener.onMetaDataReceived(s, s2);
        }

    }

    private void notifyRadioLoading() {
        Iterator i$ = this.mListenerList.iterator();

        while(i$.hasNext()) {
            RadioListener mRadioListener = (RadioListener)i$.next();
            mRadioListener.onRadioLoading();
        }

    }

    private void notifyErrorOccured() {
        Iterator i$ = this.mListenerList.iterator();

        while(i$.hasNext()) {
            RadioListener mRadioListener = (RadioListener)i$.next();
            mRadioListener.onError();
        }

    }

    private MultiPlayer getPlayer() {
        try {
            URL.setURLStreamHandlerFactory(new URLStreamHandlerFactory() {
                public URLStreamHandler createURLStreamHandler(String protocol) {
                    Log.d("LOG", "Asking for stream handler for protocol: \'" + protocol + "\'");
                    return "icy".equals(protocol)?new IcyURLStreamHandler():null;
                }
            });
        } catch (Throwable var2) {
            Log.w("LOG", "Cannot set the ICY URLStreamHandler - maybe already set ? - " + var2);
        }

        if(this.mRadioPlayer == null) {
            this.mRadioPlayer = new MultiPlayer(this, 800, 400);
            this.mRadioPlayer.setResponseCodeCheckEnabled(false);
            this.mRadioPlayer.setPlayerCallback(this);
        }

        return this.mRadioPlayer;
    }

    public boolean checkSuffix(String streamUrl) {
        return streamUrl.contains(".pls") || streamUrl.contains(".ram") || streamUrl.contains(".wax");
    }

    public void setLogging(boolean logging) {
        isLogging = logging;
    }

    private void log(String log) {
        if(isLogging) {
            Log.v("RadioManager", "RadioPlayerService : " + log);
        }

    }

    private void decodeStremLink(final String streamLink) {
        (new StreamLinkDecoder(streamLink) {
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                IAMRadioPlayerService.this.play(s);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    public void updateNotification(String singerName, String songName, int smallImage, int artImage) {
        this.singerName = singerName;
        this.songName = songName;
        this.smallImage = smallImage;
        this.artImage = BitmapFactory.decodeResource(this.getResources(), artImage);
    }

    public void updateNotification(String singerName, String songName, int smallImage, Bitmap artImage) {
        this.singerName = singerName;
        this.songName = songName;
        this.smallImage = smallImage;
        this.artImage = artImage;
    }

    public static enum State {
        IDLE,
        PLAYING,
        STOPPED;

        private State() {
        }
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public IAMRadioPlayerService getService() {
            return IAMRadioPlayerService.this;
        }
    }
}


