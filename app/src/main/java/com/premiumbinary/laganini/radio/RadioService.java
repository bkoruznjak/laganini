package com.premiumbinary.laganini.radio;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.premiumbinary.laganini.R;
import com.premiumbinary.laganini.constants.StreamConstants;
import com.premiumbinary.laganini.view.LaganiniActivity;

import java.lang.ref.WeakReference;

import co.mobiwise.library.radio.RadioListener;


/**
 * Created by bkoruznjak on 16/12/2016.
 * <p>
 * The purpose of this Service is to enable continuous music playback despite of what the user is
 * doing.
 */

public class RadioService extends Service implements RadioControl, AudioManager.OnAudioFocusChangeListener, PbPhoneStateListener, RadioListener {
    private static final int NOTIFICATION_ID = 1337;
    private final IBinder mRadioBinder = new RadioService.RadioBinder();
    private RadioStateChangeListener mStateChangeListener;
    private AudioManager mAudioManager;
    private RadioStateModel mRadioState;
    private IAMRadioManager mRadioManager;
    private TelephonyManager mTelephonyManager;
    private PhoneStateHandler mPhoneStateHandler;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;
    private String mLaganiniStreamUri = StreamConstants.STREAM_URL_HITOVI;
    private boolean shouldPlayMusic = false;
    private int mStreamVolumeHolder = 0;

    public static Intent newIntent(Context context) {
        return new Intent(context, RadioService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("bbb", "service CREATEDd");
        mRadioState = new RadioStateModel();

        mRadioManager = IAMRadioManager.with(getApplicationContext());
        mRadioManager.registerListener(this);
        mRadioManager.setLogging(true);

        if (mRadioManager != null) {
            mRadioManager.connect();
        }

        //notification construction
        Intent resultIntent = new Intent(this, LaganiniActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        0
                );
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle(getString(R.string.notification_title))
                        .setSmallIcon(R.drawable.ic_radio_notification)
                        .setOngoing(true);
        mNotificationBuilder.setContentIntent(resultPendingIntent);
        startForeground(NOTIFICATION_ID, mNotificationBuilder.build());

        mTelephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mStreamVolumeHolder = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        requestFocus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("bbb", "service DESTROYED");
        if (mRadioManager != null) {
            mRadioManager.disconnect();
        }
        if (mNotificationManager != null) {
           mNotificationManager.cancel(NOTIFICATION_ID);
       }
        abandonFocus();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("bbb", "service onStart");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mRadioBinder;
    }

    @Override
    public boolean play() {
        shouldPlayMusic = true;
        mRadioManager.startRadio(mLaganiniStreamUri);
        return false;
    }

    @Override
    public boolean pause() {
        shouldPlayMusic = false;
        mRadioManager.stopRadio();
        return false;
    }

    @Override
    public boolean stop() {
        shouldPlayMusic = false;
        mRadioManager.stopRadio();
        return false;
    }

    @Override
    public void changeStation(String radioStationUri) {
        this.mLaganiniStreamUri = radioStationUri;
        if (shouldPlayMusic) {
            mRadioManager.startRadio(radioStationUri);
        }
    }

    @Override
    public void seekForward() {
        //todo add forward seeking
    }

    @Override
    public void seekBackward() {
        //todo add backwards seeking
    }

    @Override
    public void setVolume(float volume) {
        if (mRadioManager != null) {

        }
    }

    @Override
    public void onAudioFocusChange(int i) {
        Log.d("bbb", "audio focus caught with id:" + i);
        switch (i) {
            case AudioManager.AUDIOFOCUS_GAIN:
                if (mAudioManager != null) {
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            mStreamVolumeHolder,
                            AudioManager.FLAG_PLAY_SOUND);
                }
                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                Log.d("bbb", "lost focus");
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                Log.d("bbb", "lost focus for short time");
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:

                if (mAudioManager != null) {
                    mStreamVolumeHolder = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            1,
                            AudioManager.FLAG_PLAY_SOUND);
                }
                break;
            default:
                Log.d("bbb", "default");
                break;
        }
    }

    @Override
    public void phoneStateChanged(int phoneState) {
        if (mRadioState != null) {
            switch (phoneState) {
                case TelephonyManager.CALL_STATE_IDLE:
                    if (mRadioState.wasRadioInterrupted() && mRadioManager != null) {
                        mRadioState.setRadioInterrupted(false);
                        mRadioManager.startRadio(mLaganiniStreamUri);
                    }
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    if (mRadioState.playWhenReady() && mRadioManager != null) {
                        mRadioState.setRadioInterrupted(true);
                        mRadioManager.stopRadio();
                    }
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    if (mRadioState.playWhenReady() && mRadioManager != null) {
                        mRadioState.setRadioInterrupted(true);
                        mRadioManager.stopRadio();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public RadioStateModel getCurrentRadioState() {
        return mRadioState;
    }

    public boolean shouldPlayMusic() {
        return shouldPlayMusic;
    }

    private void requestFocus() {
        if (mAudioManager != null) {
            mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);
        }

        if (mTelephonyManager != null) {
            mPhoneStateHandler = new PhoneStateHandler(new WeakReference<PbPhoneStateListener>(this));
            mTelephonyManager.listen(mPhoneStateHandler,
                    PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    private void abandonFocus() {
        if (mAudioManager != null) {
            mAudioManager.abandonAudioFocus(this);
        }

        if (mTelephonyManager != null) {
            mPhoneStateHandler = null;
            mTelephonyManager.listen(mPhoneStateHandler,
                    PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    public void registerClient(RadioStateChangeListener stateChangeListener) {
        Log.d("bbb", "REGISTRIRAM STATE CHANGE LISTENER:" + stateChangeListener);
        mStateChangeListener = stateChangeListener;
    }

    public void unRegisterClient() {
        Log.d("bbb", "DE REGISTRIRAM STATE CHANGE LISTENER:" + mStateChangeListener);
        mStateChangeListener = null;
    }

    @Override
    public void onRadioLoading() {
        Log.d("bbb", "RADIO LOADING CALLED :" + mStateChangeListener);
        mRadioState.setLoading(true);

        if (mStateChangeListener != null) {
            Log.d("bbb", "NOTIFY-AM STATE CHANGE LISTENER:" + mStateChangeListener);
            mStateChangeListener.notifyViewWithUpdates(mRadioState);
        }
    }

    @Override
    public void onRadioConnected() {
        Log.d("bbb", "RADIO CONNECTED:" + mStateChangeListener);
    }

    @Override
    public void onRadioStarted() {
        mRadioState.setLoading(false);
        if (mStateChangeListener != null) {
            mStateChangeListener.notifyViewWithUpdates(mRadioState);
        }

        mRadioState.setPlayWhenReady(true);
        if (mStateChangeListener != null) {
            mStateChangeListener.notifyViewWithUpdates(mRadioState);
        }

    }

    @Override
    public void onRadioStopped() {
        Log.d("bbb", "RADIO STOPPED");
        mRadioState.setPlayWhenReady(false);
        if (mStateChangeListener != null) {
            mStateChangeListener.notifyViewWithUpdates(mRadioState);
        }
    }

    @Override
    public void onMetaDataReceived(String s, String s1) {

        if ("StreamTitle".equals(s)) {
            mRadioState.setSongName(s1);
            Log.d("BBB", "POSTAVLJAM SONGNAME:" + s1);
            if (mStateChangeListener != null) {
                mStateChangeListener.notifyViewWithUpdates(mRadioState);
            }

            if (mNotificationBuilder != null && mNotificationManager != null) {
                mNotificationBuilder.setContentText(mRadioState.getSongName());
                mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
            }
        }
    }

    @Override
    public void onError() {
        Log.e("bbb", "ERROR HAPPENED");
        mRadioState.setPlayWhenReady(false);
        mRadioState.setRadioInterrupted(true);
        if (mStateChangeListener != null) {
            mStateChangeListener.notifyViewWithUpdates(mRadioState);
        }
    }

    public class RadioBinder extends Binder {
        public RadioService getServiceInstance() {
            return RadioService.this;
        }
    }
}
