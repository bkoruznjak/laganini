package com.premiumbinary.laganini.radio;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import co.mobiwise.library.radio.IRadioManager;
import co.mobiwise.library.radio.RadioListener;


public class IAMRadioManager implements IRadioManager {
    private static boolean isLogging = false;
    private static com.premiumbinary.laganini.radio.IAMRadioManager instance = null;
    private static IAMRadioPlayerService mService;
    private Context mContext;
    private List<RadioListener> mRadioListenerQueue;
    private boolean isServiceConnected;
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName arg0, IBinder binder) {
            com.premiumbinary.laganini.radio.IAMRadioManager.this.log("Service Connected.");
            com.premiumbinary.laganini.radio.IAMRadioManager.mService = ((IAMRadioPlayerService.LocalBinder)binder).getService();
            com.premiumbinary.laganini.radio.IAMRadioManager.mService.setLogging(com.premiumbinary.laganini.radio.IAMRadioManager.isLogging);
            com.premiumbinary.laganini.radio.IAMRadioManager.this.isServiceConnected = true;
            if(!com.premiumbinary.laganini.radio.IAMRadioManager.this.mRadioListenerQueue.isEmpty()) {
                Iterator i$ = com.premiumbinary.laganini.radio.IAMRadioManager.this.mRadioListenerQueue.iterator();

                while(i$.hasNext()) {
                    RadioListener mRadioListener = (RadioListener)i$.next();
                    com.premiumbinary.laganini.radio.IAMRadioManager.this.registerListener(mRadioListener);
                    mRadioListener.onRadioConnected();
                }
            }

        }

        public void onServiceDisconnected(ComponentName arg0) {
        }
    };

    private IAMRadioManager(Context mContext) {
        this.mContext = mContext;
        this.mRadioListenerQueue = new ArrayList();
        this.isServiceConnected = false;
    }

    public static com.premiumbinary.laganini.radio.IAMRadioManager with(Context mContext) {
        if(instance == null) {
            instance = new com.premiumbinary.laganini.radio.IAMRadioManager(mContext);
        }

        return instance;
    }

    public static IAMRadioPlayerService getService() {
        return mService;
    }

    public void startRadio(String streamURL) {
        mService.play(streamURL);
    }

    public void stopRadio() {
        mService.stop();
    }

    public boolean isPlaying() {
        this.log("IsPlaying : " + mService.isPlaying());
        return mService.isPlaying();
    }

    public void registerListener(RadioListener mRadioListener) {
        if(this.isServiceConnected) {
            mService.registerListener(mRadioListener);
        } else {
            this.mRadioListenerQueue.add(mRadioListener);
        }

    }

    public void unregisterListener(RadioListener mRadioListener) {
        this.log("Register unregistered.");
        mService.unregisterListener(mRadioListener);
    }

    public void setLogging(boolean logging) {
        isLogging = logging;
    }

    public void connect() {
        this.log("Requested to connect service.");
        Intent intent = new Intent(this.mContext, IAMRadioPlayerService.class);
        this.mContext.bindService(intent, this.mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    public void disconnect() {
        this.log("Service Disconnected.");
        this.mContext.unbindService(this.mServiceConnection);
    }

    public void updateNotification(String singerName, String songName, int smallArt, int bigArt) {
        if(mService != null) {
            mService.updateNotification(singerName, songName, smallArt, bigArt);
        }

    }

    public void updateNotification(String singerName, String songName, int smallArt, Bitmap bigArt) {
        if(mService != null) {
            mService.updateNotification(singerName, songName, smallArt, bigArt);
        }

    }

    private void log(String log) {
        if(isLogging) {
            Log.v(" bbb", "RadioManagerLog : " + log);
        }

    }
}
