package com.premiumbinary.laganini.radio;

/**
 * Created by bkoruznjak on 20/12/2016.
 */

public interface RadioStateChangeListener {
    void notifyViewWithUpdates(RadioStateModel radioStateModel);
}
