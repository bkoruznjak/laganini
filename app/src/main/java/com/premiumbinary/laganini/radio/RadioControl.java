package com.premiumbinary.laganini.radio;

/**
 * Created by bkoruznjak on 20/12/2016.
 */

public interface RadioControl {

    boolean play();

    boolean pause();

    boolean stop();

    void changeStation(String radioStationUri);

    void seekForward();

    void seekBackward();

    void setVolume(float volume);

}
