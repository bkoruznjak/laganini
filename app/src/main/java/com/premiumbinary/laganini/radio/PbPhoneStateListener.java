package com.premiumbinary.laganini.radio;

/**
 * Created by bkoruznjak on 21/12/2016.
 */

public interface PbPhoneStateListener {
    void phoneStateChanged(int phoneState);
}
