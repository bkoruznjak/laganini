package com.premiumbinary.laganini.radio;

/**
 * Created by bkoruznjak on 20/12/2016.
 */

public class RadioStateModel {
    private boolean wasRadioInterrupted;
    private boolean playWhenReady;
    private boolean isLoading;
    private int playbackState;

    private String songName = "";
    private String songAuthor = "Unknown";
    private String songTitle = "Unknown";
    private String radioName = "Unknown";

    public RadioStateModel() {
        this.wasRadioInterrupted = false;
        this.playWhenReady = false;
        this.isLoading = false;
        this.playbackState = 0;
    }

    public RadioStateModel(boolean wasRadioInterrupted, boolean playWhenReady, boolean isLoading, int playbackState) {
        this.wasRadioInterrupted = wasRadioInterrupted;
        this.playWhenReady = playWhenReady;
        this.isLoading = isLoading;
        this.playbackState = playbackState;
    }

    public boolean wasRadioInterrupted() {
        return wasRadioInterrupted;
    }

    public void setRadioInterrupted(boolean wasRadioInterrupted) {
        this.wasRadioInterrupted = wasRadioInterrupted;
    }

    public boolean playWhenReady() {
        return playWhenReady;
    }

    public void setPlayWhenReady(boolean playWhenReady) {
        this.playWhenReady = playWhenReady;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public int getPlaybackState() {
        return playbackState;
    }

    public void setPlaybackState(int playbackState) {
        this.playbackState = playbackState;
    }

    public String getSongAuthor() {
        return songAuthor;
    }

    public void setSongAuthor(String songAuthor) {
        this.songAuthor = songAuthor;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getRadioName() {
        return radioName;
    }

    public void setRadioName(String radioName) {
        this.radioName = radioName;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }
}
