package com.premiumbinary.laganini.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.premiumbinary.laganini.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by bkoruznjak on 22/12/2016.
 */

public class FeatherView extends SurfaceView {

    private static final int TARGET_FPS = 25;
    private static final int NUMBER_OF_FEATHERS = 10;
    public boolean isRunning = false;
    private FeatherView.RenderThread mRenderThread;
    private SurfaceHolder mSurfaceHolder;
    private float mScreenDensity;
    private float mTargetFrameDrawTime;
    private int holderWidth;
    private int holderHeight;
    private Bitmap featherBitmapGeneric;
    private Bitmap featherBitmapBlue;
    private Bitmap featherBitmapGrey;
    private Bitmap featherBitmapLilac;
    private Bitmap featherBitmapPurple;
    private Bitmap featherBitmapTeal;
    private Bitmap featherBitmapTurquoise;
    private Bitmap featherBitmapViolet;
    private ArrayList<Feather> featherArrayList = new ArrayList<>();
    private ArrayList<Bitmap> featherBitmapList = new ArrayList<>();


    public FeatherView(Context context) {
        super(context);
        init();
    }

    public FeatherView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FeatherView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FeatherView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        this.setDrawingCacheEnabled(true);
        mScreenDensity = getResources().getDisplayMetrics().density;
        mTargetFrameDrawTime = 1000f / TARGET_FPS;
        mSurfaceHolder = getHolder();
        mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                holderWidth = mSurfaceHolder.getSurfaceFrame().right;
                holderHeight = mSurfaceHolder.getSurfaceFrame().bottom;
                featherBitmapGeneric = BitmapFactory.decodeResource(getResources(),
                        R.drawable.img_feather_generic);
                featherBitmapBlue = BitmapFactory.decodeResource(getResources(),
                        R.drawable.img_feather_blue);
                featherBitmapGrey = BitmapFactory.decodeResource(getResources(),
                        R.drawable.img_feather_grey);
                featherBitmapLilac = BitmapFactory.decodeResource(getResources(),
                        R.drawable.img_feather_lilac);
                featherBitmapPurple = BitmapFactory.decodeResource(getResources(),
                        R.drawable.img_feather_purple);
                featherBitmapTeal = BitmapFactory.decodeResource(getResources(),
                        R.drawable.img_feather_teal);
                featherBitmapTurquoise = BitmapFactory.decodeResource(getResources(),
                        R.drawable.img_feather_turquoise);
                featherBitmapViolet = BitmapFactory.decodeResource(getResources(),
                        R.drawable.img_feather_violet);
                featherBitmapList.add(featherBitmapGeneric);
                featherBitmapList.add(featherBitmapBlue);
                featherBitmapList.add(featherBitmapGrey);
                featherBitmapList.add(featherBitmapLilac);
                featherBitmapList.add(featherBitmapPurple);
                featherBitmapList.add(featherBitmapTeal);
                featherBitmapList.add(featherBitmapTurquoise);
                featherBitmapList.add(featherBitmapViolet);

                Random randomFeatherIndex = new Random();

                for (int i = 0; i < NUMBER_OF_FEATHERS; i++) {
                    Bitmap featherBitmap = featherBitmapList.get(randomFeatherIndex.nextInt(featherBitmapList.size() - 1));
                    int scale = new Random().nextInt(4);
                    float scaleReduced = -(((float) scale / 10) - 1);
                    Feather feather = new Feather(holderWidth, holderHeight, featherBitmap, scaleReduced);
                    featherArrayList.add(feather);
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder,
                                       int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                featherBitmapList.clear();
                featherArrayList.clear();
                featherBitmapGeneric = null;
                featherBitmapBlue = null;
                featherBitmapGrey = null;
                featherBitmapLilac = null;
                featherBitmapPurple = null;
                featherBitmapTeal = null;
                featherBitmapTurquoise = null;
                featherBitmapViolet = null;

            }
        });
    }

    public void start() {
        if (isRunning) {
            return;
        }
        if (mRenderThread == null || !mRenderThread.isAlive())
            mRenderThread = new FeatherView.RenderThread(new WeakReference<SurfaceHolder>(getHolder()));
        isRunning = true;
        mRenderThread.start();
    }


    public void stop() {
        isRunning = false;
        if (mRenderThread != null) {
            mRenderThread.interrupt();
        }
        mRenderThread = null;
    }

    private class RenderThread extends Thread {
        private WeakReference<SurfaceHolder> surfaceHolder;

        private long startTimeCurrentFrame;
        private long delta;
        private long endTimeCurrentFrame;
        private long sleepTimeInMillis;

        RenderThread(WeakReference<SurfaceHolder> surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
            this.surfaceHolder.get().setFormat(PixelFormat.TRANSLUCENT);
        }

        @Override
        public void run() {
            while (isRunning) {
                if (surfaceHolder != null && surfaceHolder.get().getSurface().isValid()) {
                    try {
                        startTimeCurrentFrame = System.nanoTime() / 1000000;
                        Canvas canvas = surfaceHolder.get().lockCanvas();
                        if (canvas == null) {
                            continue;
                        }
                        draw(canvas);
                        surfaceHolder.get().unlockCanvasAndPost(canvas);
                        //FPS limit
                        endTimeCurrentFrame = System.nanoTime() / 1000000;
                        delta = endTimeCurrentFrame - startTimeCurrentFrame;
                        sleepTimeInMillis = (long) (mTargetFrameDrawTime - delta);
                        if (sleepTimeInMillis > 0) {
                            Thread.sleep(sleepTimeInMillis);
                        }
                    } catch (Exception e) {
                        Log.e("bbb", "error:" + e);
                    }
                }
            }
        }

        private void draw(Canvas canvas) {
            canvas.drawColor(Color.WHITE);
            for (Feather feather : featherArrayList) {
                feather.moveWithResetControl();
                feather.swingFeather();
                canvas.drawBitmap(feather.getFeatherBitmap(), feather.getmFeatherMatrix(), null);
            }
        }
    }

    private class Feather {

        private static final float SIZE_SCALE = 2.74f;
        private static final int ORIGINAL_FEATHER_IMAGE_WIDTH = 211;
        private static final int ORIGINAL_FEATHER_IMAGE_HEIGHT = 77;
        private Bitmap featherBitmap;


        private Matrix mFeatherMatrix = new Matrix();
        private int startingPositionX;
        private int startingPositionY;
        private int currentPositionY;
        private int fallingVelocity;
        private int screenHeight;
        private int screenWidth;
        private int mHeight;
        private int mWidth;
        private float rotateAngle;
        private float mScale;
        private float mRotationDifferentiationCoefficient;
        private boolean leftSwing;

        private Feather(int screenWidth, int screenHeight, Bitmap featherBitmap, float scale) {
            this.screenHeight = screenHeight;
            this.screenWidth = screenWidth;
            this.featherBitmap = featherBitmap;
            this.mScale = scale;
            this.mRotationDifferentiationCoefficient = (float) (new Random().nextInt(50)) / 1000;
            mHeight = (int) (ORIGINAL_FEATHER_IMAGE_HEIGHT * mScreenDensity);
            mWidth = (int) (ORIGINAL_FEATHER_IMAGE_WIDTH * mScreenDensity);
            startingPositionX = new Random().nextInt(screenWidth);
            startingPositionY = new Random().nextInt(screenHeight);
            fallingVelocity = new Random().nextInt(1) + 1;
            currentPositionY = 0 - (startingPositionY + mHeight);
        }

        public void moveWithResetControl() {
            if (currentPositionY > screenHeight + mHeight) {
                startingPositionX = new Random().nextInt(screenWidth);
                currentPositionY = 0 - (startingPositionY + mHeight);
            } else {
                currentPositionY += fallingVelocity;
            }
        }

        public void swingFeather() {
            if (leftSwing) {
                rotateAngle += (1.1f - mScale - mRotationDifferentiationCoefficient);
                if (rotateAngle > 10) {
                    leftSwing = false;
                }

            } else {
                rotateAngle -= (1.1f - mScale - mRotationDifferentiationCoefficient);
                if (rotateAngle < -35) {
                    leftSwing = true;
                }
            }
            mFeatherMatrix.postRotate(rotateAngle);
            mFeatherMatrix.setTranslate(-featherBitmap.getWidth() / 2f, featherBitmap.getHeight());
            mFeatherMatrix.preScale(mScale, mScale);
            mFeatherMatrix.postRotate(rotateAngle);
            mFeatherMatrix.postTranslate(startingPositionX, currentPositionY);
        }

        public Bitmap getFeatherBitmap() {
            return this.featherBitmap;
        }

        public Matrix getmFeatherMatrix() {
            return this.mFeatherMatrix;
        }
    }
}