package com.premiumbinary.laganini.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by bkoruznjak on 26/12/2016.
 */

public class DroidSansTextViewLight extends TextView {

    public DroidSansTextViewLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public DroidSansTextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DroidSansTextViewLight(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/neutradisplay-light.otf");
            setTypeface(tf);
        }
    }

}