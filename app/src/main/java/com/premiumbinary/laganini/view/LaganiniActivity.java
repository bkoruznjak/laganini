package com.premiumbinary.laganini.view;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.premiumbinary.laganini.R;
import com.premiumbinary.laganini.databinding.ActivityLaganiniBinding;
import com.premiumbinary.laganini.radio.RadioService;
import com.premiumbinary.laganini.radio.RadioStateChangeListener;
import com.premiumbinary.laganini.radio.RadioStateModel;

public class LaganiniActivity extends AppCompatActivity implements RadioStateChangeListener {

    private static final String SONG_BUNDLE_KEY = "SONG_NAME_KEY";

    private ActivityLaganiniBinding mBinding;
    private Intent serviceIntent;
    private RadioService mRadioService;
    private boolean isServiceBound;
    private Animation mInfiniteRotateAnimation;

    //service and state members
    private boolean shouldBePlaying;
    private boolean isLoading;
    private boolean playWhenReady;
    private String mSongName;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            RadioService.RadioBinder binder = (RadioService.RadioBinder) service;
            Log.d("bbb", "service connected again WITH:" + LaganiniActivity.this);
            mRadioService = binder.getServiceInstance();
            mRadioService.registerClient(LaganiniActivity.this);
            shouldBePlaying = mRadioService.shouldPlayMusic();
            isLoading = mRadioService.getCurrentRadioState().isLoading();
            playWhenReady = mRadioService.getCurrentRadioState().playWhenReady();
            updateTheView();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.d("bbb", "service disconnected again");
            mRadioService.unRegisterClient();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_laganini);
        mBinding.radioControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shouldBePlaying) {
                    mRadioService.pause();
                    shouldBePlaying = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mBinding.radioControlButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_png));
                        }
                    });
                } else {
                    mRadioService.play();
                    shouldBePlaying = true;
                }
            }
        });

        setSupportActionBar(mBinding.laganiniToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        serviceIntent = RadioService.newIntent(this);
        mInfiniteRotateAnimation = AnimationUtils.loadAnimation(this, R.anim.infinite_rotation);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startService(serviceIntent);
        if (!mBinding.featherSurfaceView.isRunning) {
            mBinding.featherSurfaceView.start();
        }
        isServiceBound = bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBinding.featherSurfaceView.isRunning) {
            mBinding.featherSurfaceView.stop();
        }
        if (shouldBePlaying) {
            unbindService(false);
        } else {
            unbindService(true);
        }
    }

    private void unbindService(boolean stopService) {
        if (mRadioService != null) {
            mRadioService.unRegisterClient();
            if (stopService) {
                mRadioService.stop();
            }
        }

        if (isServiceBound) {
            Log.d("bbb", "UNBIND SERVICE CALLED");
            unbindService(mConnection);
            isServiceBound = false;
        }

        if (stopService) {
            Log.d("bbb", "STOPPING RADIO SERVICE");
            stopService(serviceIntent);
            System.exit(0);
        }
    }

    @Override
    public void notifyViewWithUpdates(RadioStateModel radioStateModel) {
        //Log.d("bbb", "__________________ GOT UPDATE __________________");
        //Log.d("bbb", "LOADING:" + radioStateModel.isLoading() + ", PLAY WHEN READY:" + radioStateModel.playWhenReady());
        isLoading = radioStateModel.isLoading();
        playWhenReady = radioStateModel.playWhenReady();
        updateTheView();
    }

    private void updateTheView() {
        if (mBinding.radioControlButton != null) {
            if (isLoading) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.radioControlButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_loading_png));
                        mBinding.radioControlButton.setClickable(false);
                        mBinding.radioControlButton.startAnimation(mInfiniteRotateAnimation);
                    }
                });
            } else if (playWhenReady) {
                Log.d("bbb", "PAUSE NA BUTTONU, PLAYWHEN READY:" + playWhenReady);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.radioControlButton.clearAnimation();
                        mBinding.radioControlButton.setClickable(true);
                        mBinding.radioControlButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_png));
                    }
                });
                shouldBePlaying = true;
            } else {
                Log.d("bbb", "PLAY NA BUTTONU, PLAYWHEN READY:" + playWhenReady);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.radioControlButton.clearAnimation();
                        mBinding.radioControlButton.setClickable(true);
                        mBinding.radioControlButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_png));
                    }
                });
                shouldBePlaying = false;
            }
        }

        if (mBinding.textSongInfo != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSongName = mRadioService.getCurrentRadioState().getSongName();
                    Log.d("bbb", "SONGNAME:" + mSongName);
                    mBinding.textSongInfo.setText(mSongName);
                }
            });
        }
    }
}