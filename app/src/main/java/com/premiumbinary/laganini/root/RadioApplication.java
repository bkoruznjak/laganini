package com.premiumbinary.laganini.root;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by bkoruznjak on 20/12/2016.
 */

public class RadioApplication extends Application {

    //private static RadioApplication INSTANCE;
    //private LaganiniActivity view;

    @Override
    public void onCreate() {
        super.onCreate();
        //INSTANCE = this;
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
    }
/**
 public static RadioApplication getInstance(){
 return INSTANCE;
 }

 public void setMainView(LaganiniActivity view){
 if (this.view != null){
 this.view = null;
 }
 this.view = view;
 }

 public LaganiniActivity getMainView(){
 return this.view;
 }
 **/
}