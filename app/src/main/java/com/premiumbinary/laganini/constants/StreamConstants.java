package com.premiumbinary.laganini.constants;

/**
 * Created by bkoruznjak on 11/01/2017.
 */

public class StreamConstants {

    public static String STREAM_URL_HITOVI = "http://s7.iqstreaming.com:9498";
    public static String STREAM_URL_MORE = "http://s7.iqstreaming.com:8056";
    public static String STREAM_URL_STARI_VAL = "http://s7.iqstreaming.com:8054";
    public static String STREAM_URL_TAMBURE = "http://s7.iqstreaming.com:8058";
    public static String STREAM_URL_TOP_20 = "http://s7.iqstreaming.com:8048";

}